import ForgeUI, { render, Fragment, Text, IssuePanel, TextField, Button, Form, useState, useEffect } from '@forge/ui';
import { storage } from '@forge/api';

const App = () => {
    const [storedText, setStoredText] = useState("");
    useEffect( async () => {
        const textInput = await storage.get('text-input')
        console.log(textInput)
        setStoredText(textInput)  
    } , [storedText]);

    console.log("message console")

    const saveData = async  (data) => {
        console.log(data)
        storage.set('text-input' , data["text-input"]);
        setStoredText(data["text-input"])
    }
  
    return (
    <Fragment>
      <Form onSubmit={saveData}>
        <TextField name='text-input' label='text-input' defaultValue={storedText}/>
      </Form>
    </Fragment>
  );
};

export const run = render(
  <IssuePanel>
   <App/>
  </IssuePanel>
);
